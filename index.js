console.log("Hello World!");
// enclose string data with quotation mark

console. log("Hello world!");
// not too sensitive with space in the sytax

console.
log
(
	"Hello, everyone!"
)
// This one stilld displays XD

// ; delimeter - is used to end the code/line

// [COMMENTS]
// - single line comments

// I am a single line comment ctrl + /

/* Multi-line comment*/
/* THIS IS
A MULTI LINE
COMMENT ctrl + shift + /
*/

/* Syntax and statement
STATEMENTS in programming are instructions we give to pir computer to perform
SYNTAX in programming is the set of rules that describes how statements must be considered*/

/* VARIABLES -used to contain data
	-syntax in declaring variables
	- let/const variableName
*/
let myVariable = "Hello";
console.log(myVariable);
//console.log(hello); // will result to not defined error

// let temperature = ; //example for number 4

/*
Guides in writing variables:
	1. Use the 'let' keyword followed by the variable name of your choosingand use the assignment operator (=) to assign a value.

	2. Variale name should start with a lowercase character, use camelCasefor multiple words.

	3. For constant variables, use the 'const' keyword

	4. Variable name should be indicative or descriptive of the value being stored.

	5. Never name a variable starting with numbers.

	6. Refrain from using space in declaring a variable.
*/

// String
let productName = 'desktop computer'; //can also use single quote
console.log(productName);

let product = "Alvin's computer"
console.log(product);

// Number
let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

// Reassigning variable's value
// Syntax:
	// variableName = newValue;
	// do not use 'let' when reassigning

productName ="Laptop"
console.log(productName);

let friend = "Kate";
friend = "Jane";
console.log(friend);

//interest = 4.89;

const pi = 3.14;
//pi = 3.16; this line will cause an error

// Reassigning - a variable have a value and we re-assign a new one
// Initialize -  it is our first saving of a value

let supplier; //declaration part
supplier = "John Smith Tradings"; //initializing part
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// Multiple variable declaration

let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand);

// Using a variable with a reserved keyword
 //const let = "hello";
 //console.log(let);

/* [SECTION] Data Types
 	STRINGS - are series of characters that create a word, phrase, sentence, or anything related to creating text.
 	- Strings in JavaScript is enclosed with single (' ') or double (" ") quote
*/
let country = 'Philippines';
let province = "Metro Manila";

console.log(province + ', ' + country);

// + symbol is use to concatenate data/vallue

let fullAddress = province + ', ' + country;
console.log(fullAddress);

console.log("Philippines" + ', ' + "Metro Manila");

/* ESCAPE CHARACTERS
\n - New line
Examples below*/

console.log("line1\nline2");// \n
let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);
let message = "John's employee went home early."
console.log(message);
message = 'John\'s employee went home early.'; // \ (backslack) escape charater to diplay the single
console.log(message);

// NUMBERS

// Integers / whole number
let headcount = 26;
console.log(headcount);

// Decimal numbers / float / fraction
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e1;
console.log(planetDistance);

console.log ("John's grade last quarter is "+ grade);

// ARRAYS - used to store multiple values with similar data type

let grades = [98.7, 95.4, 90.2, 94.6];
	// array name   // array value
console.log(grades);

/* DIFFERENT DATA TYPES
Storing different data types inside an array is not recommended because it will not make sense in the context of programming*/
let details = ["John", "Smith", 32, true];
console.log(details);

/* OBJECTS
	-another special kind of data type that is used to mimic real world objects or items.

	SYNTAX:

	let/const objectName = {
		propertyA: value,
		propertyB: value
	}
*/

let person = {
	fullName: "Juan dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["0912 345 6789", "8123 7444"],
	/*address: {
		houseNumber: "345",
		city: "Manila"
	}*/
};

// person (object), fullName, age, ...(key/property), Juan dela Cruz. 35, ... (property value)

const myGrades ={
	firstGrading: 98.7,
	secondGrading: 95.4,
	thirdGrading: 90.2,
	fourthGrading: 94.6
};
console.log(myGrades);

let stringValue = "string";
let numberValue = 10;
let booleanValue = true;
let waterBills = [1000, 640, 700];
//mygrades as object

/* 'type of' operator
-used to retrieve / know the data type
*/

console.log(typeof stringValue); // output: string
console.log(typeof numberValue); //output: number
console.log(typeof booleanValue); // output: boolean
console.log(typeof waterBills); // output: object
console.log(typeof myGrades); // output: object

// CONSTANT OBJECTS AND ARRAYS
// we cannot reassign the value of the variable  but we can change the element of the constant array

const anime = ["Boruto", "One Piece", "Code Geas", "Monster", "Dan Machi", "Demon Slayer", "AOT", "Fairy Tale"];
				// 9 		1 				2 			3 			4    		5
// index - is the psition of the element starting at zero

anime [0] = "Naruto";
console.log(anime);

/* NULL - none or empty
	-used to intentionally express the absence of a value*/

let spouse = null;
console.log(spouse);

/* UNDEFINED
 - represents the state of a variable that has been declared but without an assigned value*/

 let fullName; //declaration
 console.log(fullName);

 // :)